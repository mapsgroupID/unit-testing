package it.mapsgroup.tutorial;

import java.util.*;
import java.util.function.Predicate;

import static java.util.stream.Collectors.*;

/**
 * A service class that needs refactoring.
 *
 * @author Fabio G. Strozzi
 */
public class NeedForRefactoring {
    private final List<Long> calculatedValues = new ArrayList<>();
    private final RandomNumberGenerator rng = new RandomNumberGenerator(new Random());
    private boolean initialized = false;

    /**
     * Initialize this service.
     */
    public void init() {
        if (initialized) {
            return;
        }
        try {
            System.out.println("Doing some very heavy, necessary work...");
            Thread.sleep(10_000L);
        } catch (InterruptedException ignored) { } finally { this.initialized = true; }
    }

    /**
     * Clears the calculated values.
     */
    public void clear() {
        this.initialized = false;
        calculatedValues.clear();
    }

    /**
     * Performs some sort of calculation using sliding windows.
     *
     * @param nr     The number of values to be calculated
     * @param window The size of the sliding window
     */
    public void performCalculations(int nr, int window) {
        init();
        for (int i = 0; i < nr; i++) {
            long n = calculatedValues.isEmpty() ? 0 : calculatedValues.get(calculatedValues.size() - 1);
            long offset = (long) i * window;
            long value = rng.nextBetween(offset, offset + window);
            calculatedValues.add(n + value);
        }
    }

    /**
     * Calculated values can be searched.
     *
     * @param predicate A predicate
     * @return The list of matching values
     */
    public List<Long> search(Predicate<Long> predicate) {
        return calculatedValues.stream().filter(predicate).collect(toList());
    }
}
