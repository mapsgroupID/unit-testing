package it.mapsgroup.tutorial;

import java.util.Random;

/**
 * A random number generator.
 *
 * @author Fabio G. Strozzi
 */
public class RandomNumberGenerator {

    private final Random random;

    public RandomNumberGenerator(Random random) { this.random = random; }

    public long nextBetween(long min, long max) {
        return random.nextInt((int)(max - min)) + min;
    }
}
