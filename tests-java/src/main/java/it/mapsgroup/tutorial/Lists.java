package it.mapsgroup.tutorial;

import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * @author Fabio G. Strozzi
 */
public class Lists {
    private Lists() {}

    /**
     * Searches the list for an element that satisfies the predicate; if one is found that value is returned,
     * otherwise the given supplier is used to create a new value that is first appended to the list and
     * then returned.
     *
     * @param ts   The list
     * @param pred The predicated used to search for the required value
     * @param supp The supplier of the new value.
     * @param <T>  The type of elements.
     * @return Either an existing value or the supplier generated one after being appended to the list.
     */
    public static <T> T searchOrAddIfAbsent(List<T> ts, Predicate<? super T> pred, Supplier<? extends T> supp) {
        Objects.requireNonNull(ts, "List cannot be null");
        Optional<T> maybe = ts.stream().filter(pred).findFirst();
        if (maybe.isPresent())
            return maybe.get();
        T t = supp.get();
        ts.add(t);
        return t;
    }
}
