package it.mapsgroup.tutorial;

import java.util.List;

import static it.mapsgroup.tutorial.CurveService.Tuple2.*;
import static java.util.stream.Collectors.*;
import static java.util.stream.IntStream.*;

/**
 * A service that works with discrete curves.
 *
 * @author Fabio G. Strozzi
 */
public class CurveService {
    private final RandomNumberGenerator generator;
    private final DiscreteCurve curve;

    public CurveService(RandomNumberGenerator generator,
                        DiscreteCurve curve) {
        this.generator = generator;
        this.curve = curve;
    }

    /**
     * Samples a certain number of values between a range of coordinates.
     *
     * @param n   The number of samples
     * @param min The range lower bound
     * @param max The range upper bound
     * @return The
     */
    public List<XY> sampleValues(int n, long min, long max) {
        return range(0, n)
                .boxed()
                .map(i -> generator.nextBetween(min, max))
                .map(x -> tuple(x, curve.valueAtPoint(x)))
                .filter(t -> t.second.isPresent())
                .map(t -> new XY(t.first, t.second.get()))
                .collect(toList());
    }

    public static final class Tuple2<S, T> {
        public final S first;
        public final T second;

        public Tuple2(S first, T second) {
            this.first = first;
            this.second = second;
        }

        static <S, T> Tuple2<S, T> tuple(S s, T t) {
            return new Tuple2<>(s, t);
        }
    }

    public static class XY {
        public final long x;
        public final long y;

        public XY(long x, long y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof XY)) return false;

            XY xy = (XY) o;

            if (x != xy.x) return false;
            return y == xy.y;
        }

        @Override
        public int hashCode() {
            int result = (int) (x ^ (x >>> 32));
            result = 31 * result + (int) (y ^ (y >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return "XY{" +
                   "x=" + x +
                   ", y=" + y +
                   '}';
        }
    }
}
