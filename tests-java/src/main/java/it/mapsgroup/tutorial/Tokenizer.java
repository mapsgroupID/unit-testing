package it.mapsgroup.tutorial;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

/**
 * A stateless tokenizer of arithmetic expressions.
 */
public class Tokenizer {

    /**
     * Parses the given text and returns the list of tokens in the expression.
     * <p>
     * Example:
     * </p>
     * <pre>{@code
     * Ast ast = new Parser().parseExpression("18 - 2 + 5.3");
     * }</pre>
     *
     * @param text An arithmetic expression with two operators: <code>+</code> and <code>-</code>.
     * @return The list of tokens, might be empty if string is blank or null
     */
    public List<Token> tokenize(String text) {
        final String[] rawTokens = text.split("\\s");
        return Arrays.stream(rawTokens).map(this::mkToken).collect(toList());
    }

    private static final Pattern NUM_REGEXP = Pattern.compile("^\\d+(?:\\.\\d+)?$");
    private static final Pattern OP_REGEXP = Pattern.compile("^\\+-$");

    private Token mkToken(String token) {
        if (NUM_REGEXP.matcher(token).matches()) {
            return new NumericToken(Float.parseFloat(token));
        }
        if (OP_REGEXP.matcher(token).matches()) {
            return new OperatorToken(token);
        }
        return null;
    }

    interface Token { }

    public static class NumericToken implements Token {
        public final float value;

        public NumericToken(float value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof NumericToken)) return false;
            NumericToken that = (NumericToken) o;
            return Float.compare(that.value, value) == 0;
        }

        @Override
        public int hashCode() {
            return (value != +0.0f ? Float.floatToIntBits(value) : 0);
        }
    }

    public static class OperatorToken implements Token {
        public final String operator;

        public OperatorToken(String operator) {
            this.operator = operator;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof OperatorToken)) return false;
            OperatorToken that = (OperatorToken) o;
            return Objects.equals(operator, that.operator);
        }

        @Override
        public int hashCode() {
            return operator != null ? operator.hashCode() : 0;
        }
    }

}
