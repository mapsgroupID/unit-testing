package it.mapsgroup.tutorial;

import java.util.Optional;

/**
 * A discrete curve in 2D.
 *
 * @author Fabio G. Strozzi
 */
public interface DiscreteCurve {

    /**
     * Returns the value of function at the given coordinate.
     *
     * @param x Coordinate
     * @return The value at x, may not be defined
     */
    Optional<Long> valueAtPoint(long x);

}
