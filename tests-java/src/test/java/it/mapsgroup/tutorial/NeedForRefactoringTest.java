package it.mapsgroup.tutorial;

import org.junit.jupiter.api.*;

/**
 * Unit tests for {@link NeedForRefactoring}.
 *
 * @author Fabio G. Strozzi
 */
class NeedForRefactoringTest {

    @BeforeAll
    static void beforeAll() {
        // This is called before everything is run
    }

    @AfterAll
    static void afterAll() {
        // This is called after all tests
    }

    @BeforeEach
    void beforeEach() {
        // This is called before each test
    }

    @AfterEach
    void afterEach() {
        // This is called after each test
    }

    /*
     * TODO Write some tests for NeedForRefactoringTest:
     *      - if you judge the class need some refactoring, then go ahead and change it!
     *      - feel free to implement lifecycle hooks
     *      - use mocking if necessary
     *      - write as many tests as you can
     */

}
