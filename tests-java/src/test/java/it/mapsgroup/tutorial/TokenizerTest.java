package it.mapsgroup.tutorial;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 * Unit tests for {@link Tokenizer}.
 *
 * @author Fabio G. Strozzi
 */
class TokenizerTest {

    @Test
    void aSimpleExpressionCanBeParsed() {
        List<Tokenizer.Token> tokens = new Tokenizer().tokenize("18 - 2 + 5.3");
        assertThat("Non null list of tokens is returned", tokens, is(notNullValue()));
        assertThat("Parsed tokens are not empty", tokens, is(not(empty())));
        assertThat("Exactly 5 tokens parsed", tokens, hasSize(5));
    }

    /*
     * TODO write more tests to make sure that:
     *      - Tokenizer works with more complex expressions
     *      - fails when it needs to
     *      - fix it, if possible, or clarify its signature
     */
}
