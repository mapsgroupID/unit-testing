package it.mapsgroup.tutorial;

import it.mapsgroup.tutorial.CurveService.XY;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.*;

import static java.util.Arrays.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * Unit tests for {@link CurveServiceTest}.
 *
 * @author Fabio G. Strozzi
 */
class CurveServiceTest {

    @Test
    void sampleTheSameValueOnALine() {
        // create mocks
        RandomNumberGenerator rng = mock(RandomNumberGenerator.class);
        DiscreteCurve curve = mock(DiscreteCurve.class);

        // program their behaviour
        when(rng.nextBetween(1L, 10L)).thenReturn(5L);
        when(curve.valueAtPoint(5L)).then(inv -> Optional.of(inv.getArgument(0, Long.class) * 3 + 2));

        // now make up the unit and run the test
        CurveService service = new CurveService(rng, curve);
        List<XY> actual = service.sampleValues(3, 1L, 10L);

        // now verify interactions
        // this does not prove the right order of invocation though!
        verify(rng, times(3)).nextBetween(1L, 10L);
        verify(curve, times(3)).valueAtPoint(5L);

        // and then test returned values as usual
        XY[] expected = new XY[]{new XY(5L, 17L), new XY(5L, 17L), new XY(5L, 17L)};

        assertThat("Samples are not null", actual, is(notNullValue()));
        assertThat("3 sample values are returned", actual, hasSize(3));
        assertThat("Samples are the ones expected", actual, contains(expected));
    }

    @Test
    void sampleDifferentValuesOnALine() {
        // create mocks
        RandomNumberGenerator rng = mock(RandomNumberGenerator.class);
        DiscreteCurve curve = mock(DiscreteCurve.class);

        // prepares the values to be returned by the RNG
        Deque<Long> values = new ArrayDeque<>(asList(2L, 3L, 5L, 8L));
        when(rng.nextBetween(1L, 10L)).then(inv -> values.removeFirst());

        // compute the returned value for any given long point
        when(curve.valueAtPoint(anyLong())).then(inv -> Optional.of(inv.getArgument(0, Long.class) * 3 + 2));

        // now make up the unit and run the test
        CurveService service = new CurveService(rng, curve);
        List<XY> actual = service.sampleValues(4, 1L, 10L);

        // now verify interactions with the RNG
        verify(rng, times(4)).nextBetween(1L, 10L);

        // make sure the curve is invoked with the expected random values
        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        verify(curve, times(4)).valueAtPoint(captor.capture());
        assertThat("X values passed to curve are right", captor.getAllValues(), contains(2L, 3L, 5L, 8L));

        // and then test returned values as usual
        XY[] expected = new XY[]{new XY(2L, 8L), new XY(3L, 11L), new XY(5L, 17L), new XY(8L, 26L)};

        assertThat("Samples are not null", actual, is(notNullValue()));
        assertThat("4 sample values are returned", actual, hasSize(4));
        assertThat("Samples are the ones expected", actual, contains(expected));
    }
}
