package it.mapsgroup.tutorial;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.*;

import static it.mapsgroup.tutorial.Lists.*;
import static java.util.Arrays.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for {@link Lists}.
 *
 * @author Fabio G. Strozzi
 */
class ListsTest {

    @Test
    void aValueThatIsPresentIsReturned() {
        List<String> list = new ArrayList<>(asList("A", "b", "123", "null"));
        String needle = "123";
        String res = searchOrAddIfAbsent(
                list,
                (String s) -> Objects.equals(s, needle),
                () -> needle);

        assertNotNull(res, "Searched value is always returned");
        assertEquals(res, needle, "Returned value is the expected one");
        assertEquals(4, list.size(), "List size is unchanged");
    }

    @Test
    void nullPointerExceptionIsThrownIfListIsNull() {
        String needle = "123";

        Executable test = () -> searchOrAddIfAbsent(
                null,
                (String s) -> Objects.equals(s, needle),
                () -> needle);

        NullPointerException ex = assertThrows(NullPointerException.class, test, "Passing a null list causes a NPE");
        assertEquals("List cannot be null", ex.getMessage(), "Fails because list cannot be null");
    }

    @Test
    void aValueThatIsNotPresentIsAppendedAndReturned() {
        List<String> list = new ArrayList<>(asList("A", "b", "123", "null"));
        String needle = "catch me if you can";
        String res = searchOrAddIfAbsent(
                list,
                (String s) -> Objects.equals(s, needle),
                () -> needle);

        assertNotNull(res, "Searched value is always returned");
        assertEquals(res, needle, "Returned value is the expected one");
        assertEquals(5, list.size(), "List size is increased");
        assertEquals(list.get(4), needle, "Last element is the inserted one");
    }

}
