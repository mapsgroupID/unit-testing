package it.mapsgroup.tutorial;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.*;

import static it.mapsgroup.tutorial.Lists.*;
import static java.util.Arrays.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 * Unit tests for {@link Lists}.
 *
 * @author Fabio G. Strozzi
 */
class ListsHamcrestTest {

    @Test
    void aValueThatIsPresentIsReturned() {
        List<String> list = new ArrayList<>(asList("A", "b", "123", "null"));
        String needle = "123";
        String res = searchOrAddIfAbsent(
                list,
                (String s) -> Objects.equals(s, needle),
                () -> needle);

        assertThat("Searched value is always returned", res, is(notNullValue()));
        assertThat("Returned value is the expected one", res, is(needle));
        assertThat("List size is unchanged", list, hasSize(4));
    }

    @Test
    void nullPointerExceptionIsThrownIfListIsNull() {
        // NOTE same implementation of ListsTests
    }

    @Test
    void aValueThatIsNotPresentIsAppendedAndReturned() {
        List<String> list = new ArrayList<>(asList("A", "b", "123", "null"));
        String needle = "catch me if you can";
        String res = searchOrAddIfAbsent(
                list,
                (String s) -> Objects.equals(s, needle),
                () -> needle);

        assertThat("Searched value is always returned", res, is(notNullValue()));
        assertThat("Returned value is the expected one", res, is(needle));
        assertThat("List size is increased", list, hasSize(5));
        assertThat("Last element is the inserted one", list.get(4), is(needle));
    }
}
